import { ISong } from 'app/shared/model/song.model';
import cx from 'clsx';
import React from 'react';
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Genres from './genres';
import CardActionArea from '@material-ui/core/CardActionArea';

const useStyles = makeStyles(() => ({
  root: {
    margin: 'auto',
    borderRadius: 12,
    maxWidth: 345,
  },
  media: {
    borderRadius: 12,
    height: 200,
  },
  content: {
    backgroud: '#f4f5f578'
  }
}));

export interface IArtistInfoProps {
  song?: ISong;
}

const ArtistInfo = (props: IArtistInfoProps) => {
  const cardStyles = useStyles();

  const { song } = props;

  return (
    <Card className={cx(cardStyles.root)}>
      <CardActionArea>{song ? <CardMedia className={cardStyles.media} image={song.artists[0].artistPicUrl} /> : ''}</CardActionArea>
      <CardContent className={cardStyles.content}>
        <h3>{song ? song.artists[0].artistName : ''}</h3>
        <dl className="jh-entity-details">
          <dt>Bio</dt>
          <dd>{song ? song.artists[0].bio : ''}</dd>
          <dt>Genres</dt>
          <dd>{song ? <Genres genres={song.genres} /> : ''}</dd>
          <dt>Spotify</dt>
          <dd>
            {song ? (
              <iframe
                src={`https://open.spotify.com/embed/track/${song.spotifyUrl}`}
                width="300"
                height="80"
                frameBorder="0"
                allow="encrypted-media"
              ></iframe>
            ) : (
              ''
            )}
          </dd>
        </dl>
      </CardContent>
    </Card>
  );
};

export default ArtistInfo;
