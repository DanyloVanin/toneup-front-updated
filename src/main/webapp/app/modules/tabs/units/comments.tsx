import React from 'react';
import { IComment } from 'app/shared/model/comment.model';
import Rating from '@material-ui/lab/Rating';
import { Translate, TextFormat } from 'react-jhipster';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { APP_DATE_ONLY_FORMAT } from 'app/config/constants';
import Divider from '@material-ui/core/Divider';

export interface ICommentsProps {
  comments?: IComment[];
}

const Comments = (props: ICommentsProps) => {
  const { comments } = props;

  return (
    <div className="table-responsive">
      <h5 className="comments-header">
        <Translate contentKey="toneupApp.comment.home.title">Comments</Translate>
      </h5>
      {comments && comments.length > 0 ? (
        <div>
          <List dense={true}>
            {comments.map((comment, i) => (
              <div key={`div-${i}`}>
                <ListItem key={`comment-${i}`}>
                  <ListItemAvatar>
                    <Avatar></Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      <div>
                        <Typography>
                          {comment.commentAuthor.login}
                          {comment.dateCreated ? (
                            <span className="comment-date">
                              &nbsp;
                              <TextFormat value={comment.dateCreated} type="date" format={APP_DATE_ONLY_FORMAT} />
                            </span>
                          ) : null}
                        </Typography>
                        <Rating value={comment.rating} precision={0.5} readOnly />
                      </div>
                    }
                    secondary={comment.comment}
                  />
                </ListItem>
                {(i!==comments.length-1) && <Divider variant="inset" component="li" /> }
              </div>
            ))}
          </List>
        </div>
      ) : (
        <div className="alert alert-warning">
          <Translate contentKey="toneupApp.comment.home.notFound">No comments found</Translate>
        </div>
      )}
    </div>
  );
};

export default Comments;
