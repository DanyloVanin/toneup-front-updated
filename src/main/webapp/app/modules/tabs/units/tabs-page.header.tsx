import { ISong } from 'app/shared/model/song.model';
import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export interface ITabsHeaderProps {
    song?: ISong;
}

const TabsHeader = (props: ITabsHeaderProps) => {
  const {song} = props;

  return (
    <h1 data-cy="tabsDetailsHeading">
            <Button tag={Link} to="/tabs" replace color="info" data-cy="entityDetailsBackButton" className="tabs-btn">
              <FontAwesomeIcon icon="arrow-left" />{' '}
              <span className="d-none d-md-inline">
                <Translate contentKey="entity.action.back">Back</Translate>
              </span>
            </Button>
            &nbsp;&nbsp;
            {song ? song.name : ''}
    </h1>
  );
};

export default TabsHeader;
