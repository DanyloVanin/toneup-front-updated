import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Translate, TextFormat } from 'react-jhipster';

export interface ITabsLyricsProps {
  text?: string;
}

const TabsLyrics = (props: ITabsLyricsProps) => {
  const { text } = props;

  return (
    <div>
      <h4>
        <Translate contentKey="toneupApp.tabs.lyrics">Lyrics</Translate>
      </h4>
      <Typography>{text}</Typography>
    </div>
  );
};

export default TabsLyrics;
