import React from 'react';
import { IGenre } from 'app/shared/model/genre.model';
import { Badge } from 'reactstrap';
import Typography from '@material-ui/core/Typography';

export interface IGenresProps {
  genres?: IGenre[];
}

const Genres = (props: IGenresProps) => {
  const { genres } = props;

  return (
    <div>
      {genres && genres.length > 0 ? (
        <Typography>
          {genres.map((genre, i) => (
            <span key={`genre-${i}`}>
              <Badge color="secondary">{genre.name}</Badge>&nbsp;
            </span>
          ))}
        </Typography>
      ) : (
        <div>...</div>
      )}
    </div>
  );
};

export default Genres;
