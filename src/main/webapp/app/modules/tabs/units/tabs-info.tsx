import { ISongRecord } from 'app/shared/model/song-record.model';
import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Row, Col } from 'reactstrap';
import { APP_DATE_FORMAT } from 'app/config/constants';
import { Translate, TextFormat } from 'react-jhipster';
import Rating from '@material-ui/lab/Rating';

export interface ITabsInfoProps {
  tabsEntity?: ISongRecord;
}

const TabsInfo = (props: ITabsInfoProps) => {
  const { tabsEntity } = props;

  return (
    <Col md="8">
      <Row>
        <Col md="4">
          <dl className="jh-entity-details">
            <dt>
              <span id="tonality">
                <Translate contentKey="toneupApp.tabs.tonality">Tonality</Translate>
              </span>
            </dt>
            <dd>{tabsEntity.tonality}</dd>
            <dt>
              <span id="tempo">
                <Translate contentKey="toneupApp.tabs.tempo">Tempo</Translate>
              </span>
            </dt>
            <dd>{tabsEntity.tempo}</dd>
            <dt>
              <span id="difficulty">
                <Translate contentKey="toneupApp.tabs.difficulty">Difficulty</Translate>
              </span>
            </dt>
            <dd>{tabsEntity.difficulty}</dd>
          </dl>
        </Col>
        <Col md="4">
          <dl className="jh-entity-details">
            <dt>
              <Translate contentKey="toneupApp.tabs.addedBy">Added by</Translate>
            </dt>
            <dd>{tabsEntity.author ? <Link to={`/user/${tabsEntity.author.id}`}>{tabsEntity.author.login}</Link> : ''}</dd>
            <dt>
              <span id="dateUploaded">
                <Translate contentKey="toneupApp.songRecord.dateUploaded">Date Uploaded</Translate>
              </span>
            </dt>
            <dd>{tabsEntity.dateUploaded ? <TextFormat value={tabsEntity.dateUploaded} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
            <dt>
              <span id="rating">
                <Translate contentKey="toneupApp.songRecord.rating">Rating</Translate>
              </span>
            </dt>
            <dd>{tabsEntity.rating ? <Rating value={tabsEntity.rating} precision={0.5} readOnly/> : ''}</dd>
          </dl>
        </Col>
      </Row>
    </Col>
  );
};

export default TabsInfo;
