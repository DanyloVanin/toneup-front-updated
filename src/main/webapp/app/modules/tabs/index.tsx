import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import TabList from './tabs';
import TabPage from './tab.page';

const Tabs = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TabPage} />
      <ErrorBoundaryRoute path={match.url} component={TabList} />
    </Switch>
  </>
);

export default Tabs;
