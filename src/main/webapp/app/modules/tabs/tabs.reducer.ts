import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISongRecord, defaultValue } from 'app/shared/model/song-record.model';

export const ACTION_TYPES = {
  FETCH_SONGRECORD_LIST: '/songRecord/FETCH_SONGRECORD_LIST',
  FETCH_SONGRECORD: '/songRecord/FETCH_SONGRECORD'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISongRecord>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type TabsState = Readonly<typeof initialState>;

// Reducer

export default (state: TabsState = initialState, action): TabsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SONGRECORD_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SONGRECORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_SONGRECORD_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SONGRECORD):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_SONGRECORD_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_SONGRECORD):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/public/song-records';

// Actions
export const getEntities: ICrudGetAllAction<ISongRecord> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SONGRECORD_LIST,
    payload: axios.get<ISongRecord>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ISongRecord> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SONGRECORD,
    payload: axios.get<ISongRecord>(requestUrl),
  };
};