import './tabs.scss'
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { byteSize, Translate, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Rating from '@material-ui/lab/Rating';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './tabs.reducer';
import { ISongRecord } from 'app/shared/model/song-record.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

export interface ITabsProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Tabs = (props: ITabsProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const advancedSort = p => () => {
    console.log('sord not implemented yet for: ',p);
  }

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  const { songRecordList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="song-record-heading" data-cy="SongRecordHeading" className="tabs-header">
        <div className="d-flex justify-content-end ">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.tabs.home.refreshListLabel">Refresh List</Translate>
          </Button>
          {props.isAuthenticated &&
            <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
              <FontAwesomeIcon icon="plus" />
              &nbsp;
              <Translate contentKey="toneupApp.tabs.home.createLabel">Create new tab</Translate>
            </Link>
          }
        </div>
      </h2>
      <div className="table-responsive">
        {songRecordList && songRecordList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="toneupApp.tabs.id">Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={advancedSort('SongName')}>
                  <Translate contentKey="toneupApp.tabs.songName">SongName</Translate>
                </th>
                <th className="hand" onClick={sort('rating')}>
                  <Translate contentKey="toneupApp.tabs.rating">Rating</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('dateUploaded')}>
                  <Translate contentKey="toneupApp.tabs.dateUploaded">Date Uploaded</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('difficulty')}>
                  <Translate contentKey="toneupApp.tabs.difficulty">Difficulty</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={advancedSort('added by')}>
                  <Translate contentKey="toneupApp.tabs.addedBy">Added By</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {songRecordList.map((tabsRecord, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${tabsRecord.id}`} color="link" size="sm">
                      {tabsRecord.id}
                    </Button>
                  </td>
                  <td>{tabsRecord.song ? <Link to={`tabs/${tabsRecord.song.id}`}>{tabsRecord.song.name}</Link> : ''}</td>
                  <td><Rating value={tabsRecord.rating} precision={0.5} readOnly/></td>
                  <td>
                    {tabsRecord.dateUploaded ? <TextFormat type="date" value={tabsRecord.dateUploaded} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    <Translate contentKey={`toneupApp.Difficulty.${tabsRecord.difficulty}`} />
                  </td>
                  <td>{tabsRecord.author ? <Link to={`user/${tabsRecord.author.id}`}>{tabsRecord.author.login}</Link> : ''} </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${tabsRecord.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.tabs.home.notFound">No tabs found</Translate>
            </div>
          )
        )}
      </div>
      {props.totalItems ? (
        <div className={songRecordList && songRecordList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

const mapStateToProps = ({ tabs, authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  songRecordList: tabs.entities,
  loading: tabs.loading,
  totalItems: tabs.totalItems,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);
