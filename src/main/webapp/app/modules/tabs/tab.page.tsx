import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import { getEntity as getTabs } from './tabs.reducer';
import ArtistInfo from './units/artist-info';
import TabsHeader from './units/tabs-page.header';
import TabsLyrics from './units/tabs-page.lyrics';
import Comments from './units/comments';
import TabsInfo from './units/tabs-info';

export interface ITabsPageProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TabsPage = (props: ITabsPageProps) => {
  useEffect(() => {
    props.getTabs(props.match.params.id);
  }, []);

  const { tabsEntity } = props;

  console.log(tabsEntity);

  return (
    <div>
      <Row className="tabs-heading">
        <Col md="7">
          <TabsHeader song={tabsEntity.song}></TabsHeader>
        </Col>
      </Row>
      <Row>
        <Col>
          <Row>
            <TabsInfo tabsEntity={tabsEntity}></TabsInfo>
          </Row>
          <Row className="no-margin-row">
            <TabsLyrics text="hello"></TabsLyrics>
          </Row>
          <Row className="no-margin-row">
            <Comments comments={tabsEntity.comments}></Comments>
          </Row>
        </Col>
        <Col md="5">
          <ArtistInfo song={tabsEntity.song}></ArtistInfo>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = ({ tabs }: IRootState) => ({
  tabsEntity: tabs.entity,
});

const mapDispatchToProps = { getTabs };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TabsPage);
