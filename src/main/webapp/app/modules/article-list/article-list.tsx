
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Row, Col, Alert } from 'reactstrap';

export type IArticleListProp = StateProps;

export const ArticleList = (props: IArticleListProp) => {
  const { account } = props;

  return (
    <Row>
      <Col md="8" className="pad">
        Article list section is under development!
      </Col>
    </Row>
  );
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(ArticleList);
