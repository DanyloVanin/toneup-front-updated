import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Album from './album';
import Artist from './artist';
import Genre from './genre';
import Song from './song';
import SongRecord from './song-record';
import Article from './article';

const Routes = ({ match }) => (
  <div>
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}album`} component={Album} />
      <ErrorBoundaryRoute path={`${match.url}artist`} component={Artist} />
      <ErrorBoundaryRoute path={`${match.url}genre`} component={Genre} />
      <ErrorBoundaryRoute path={`${match.url}song`} component={Song} />
      <ErrorBoundaryRoute path={`${match.url}song-record`} component={SongRecord} />
      <ErrorBoundaryRoute path={`${match.url}article`} component={Article} />
    </Switch>
  </div>
);

export default Routes;
