import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGenre } from 'app/shared/model/genre.model';
import { getEntities as getGenres } from 'app/entities/genre/genre.reducer';
import { IArtist } from 'app/shared/model/artist.model';
import { getEntities as getArtists } from 'app/entities/artist/artist.reducer';
import { getEntity, updateEntity, createEntity, reset } from './album.reducer';
import { IAlbum } from 'app/shared/model/album.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAlbumUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AlbumUpdate = (props: IAlbumUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { albumEntity, genres, artists, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/album');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getGenres();
    props.getArtists();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.releaseYear = convertDateTimeToServer(values.releaseYear);

    if (errors.length === 0) {
      const entity = {
        ...albumEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.album.home.createOrEditLabel" data-cy="AlbumCreateUpdateHeading">
            <Translate contentKey="toneupApp.album.home.createOrEditLabel">Create or edit a Album</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : albumEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="album-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="album-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="albumNameLabel" for="album-albumName">
                  <Translate contentKey="toneupApp.album.albumName">Album Name</Translate>
                </Label>
                <AvField
                  id="album-albumName"
                  data-cy="albumName"
                  type="text"
                  name="albumName"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="releaseYearLabel" for="album-releaseYear">
                  <Translate contentKey="toneupApp.album.releaseYear">Release Year</Translate>
                </Label>
                <AvInput
                  id="album-releaseYear"
                  data-cy="releaseYear"
                  type="datetime-local"
                  className="form-control"
                  name="releaseYear"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.albumEntity.releaseYear)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="album-description">
                  <Translate contentKey="toneupApp.album.description">Description</Translate>
                </Label>
                <AvField id="album-description" data-cy="description" type="text" name="description" />
              </AvGroup>
              <AvGroup>
                <Label id="albumPicUrlLabel" for="album-albumPicUrl">
                  <Translate contentKey="toneupApp.album.albumPicUrl">Album Pic Url</Translate>
                </Label>
                <AvField id="album-albumPicUrl" data-cy="albumPicUrl" type="text" name="albumPicUrl" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/album" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  genres: storeState.genre.entities,
  artists: storeState.artist.entities,
  albumEntity: storeState.album.entity,
  loading: storeState.album.loading,
  updating: storeState.album.updating,
  updateSuccess: storeState.album.updateSuccess,
});

const mapDispatchToProps = {
  getGenres,
  getArtists,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AlbumUpdate);
