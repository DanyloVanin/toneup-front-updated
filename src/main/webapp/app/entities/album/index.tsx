import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Album from './album';
import AlbumDetail from './album-detail';
import AlbumUpdate from './album-update';
import AlbumDeleteDialog from './album-delete-dialog';
import PrivateRoute from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

const Routes = ({ match }) => (
  <>
    <Switch>
      <PrivateRoute exact path={`${match.url}/new`} component={AlbumUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <PrivateRoute exact path={`${match.url}/:id/edit`} component={AlbumUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AlbumDetail} />
      <ErrorBoundaryRoute path={match.url} component={Album} />
    </Switch>
    <PrivateRoute exact path={`${match.url}/:id/delete`} component={AlbumDeleteDialog} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
  </>
);

export default Routes;
