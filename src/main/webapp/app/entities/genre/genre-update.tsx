import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISong } from 'app/shared/model/song.model';
import { getEntities as getSongs } from 'app/entities/song/song.reducer';
import { IAlbum } from 'app/shared/model/album.model';
import { getEntities as getAlbums } from 'app/entities/album/album.reducer';
import { getEntity, updateEntity, createEntity, reset } from './genre.reducer';
import { IGenre } from 'app/shared/model/genre.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IGenreUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const GenreUpdate = (props: IGenreUpdateProps) => {
  const [idssongId, setIdssongId] = useState([]);
  const [idsalbumId, setIdsalbumId] = useState([]);
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { genreEntity, songs, albums, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/genre');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getSongs();
    props.getAlbums();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...genreEntity,
        ...values,
        songIds: mapIdList(values.songIds),
        albumIds: mapIdList(values.albumIds),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.genre.home.createOrEditLabel" data-cy="GenreCreateUpdateHeading">
            <Translate contentKey="toneupApp.genre.home.createOrEditLabel">Create or edit a Genre</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : genreEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="genre-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="genre-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="genre-name">
                  <Translate contentKey="toneupApp.genre.name">Name</Translate>
                </Label>
                <AvField
                  id="genre-name"
                  data-cy="name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/genre" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  songs: storeState.song.entities,
  albums: storeState.album.entities,
  genreEntity: storeState.genre.entity,
  loading: storeState.genre.loading,
  updating: storeState.genre.updating,
  updateSuccess: storeState.genre.updateSuccess,
});

const mapDispatchToProps = {
  getSongs,
  getAlbums,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GenreUpdate);
