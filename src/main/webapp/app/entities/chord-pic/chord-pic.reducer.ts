import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IChordPic, defaultValue } from 'app/shared/model/chord-pic.model';

export const ACTION_TYPES = {
  FETCH_CHORDPIC_LIST: 'chordPic/FETCH_CHORDPIC_LIST',
  FETCH_CHORDPIC: 'chordPic/FETCH_CHORDPIC',
  CREATE_CHORDPIC: 'chordPic/CREATE_CHORDPIC',
  UPDATE_CHORDPIC: 'chordPic/UPDATE_CHORDPIC',
  PARTIAL_UPDATE_CHORDPIC: 'chordPic/PARTIAL_UPDATE_CHORDPIC',
  DELETE_CHORDPIC: 'chordPic/DELETE_CHORDPIC',
  RESET: 'chordPic/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChordPic>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ChordPicState = Readonly<typeof initialState>;

// Reducer

export default (state: ChordPicState = initialState, action): ChordPicState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHORDPIC_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHORDPIC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CHORDPIC):
    case REQUEST(ACTION_TYPES.UPDATE_CHORDPIC):
    case REQUEST(ACTION_TYPES.DELETE_CHORDPIC):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_CHORDPIC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CHORDPIC_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHORDPIC):
    case FAILURE(ACTION_TYPES.CREATE_CHORDPIC):
    case FAILURE(ACTION_TYPES.UPDATE_CHORDPIC):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_CHORDPIC):
    case FAILURE(ACTION_TYPES.DELETE_CHORDPIC):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHORDPIC_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHORDPIC):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHORDPIC):
    case SUCCESS(ACTION_TYPES.UPDATE_CHORDPIC):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_CHORDPIC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHORDPIC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/chord-pics';

// Actions

export const getEntities: ICrudGetAllAction<IChordPic> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CHORDPIC_LIST,
  payload: axios.get<IChordPic>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IChordPic> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHORDPIC,
    payload: axios.get<IChordPic>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IChordPic> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHORDPIC,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChordPic> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHORDPIC,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IChordPic> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_CHORDPIC,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChordPic> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHORDPIC,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
