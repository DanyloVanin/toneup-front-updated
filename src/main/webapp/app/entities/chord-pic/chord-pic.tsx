import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './chord-pic.reducer';
import { IChordPic } from 'app/shared/model/chord-pic.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChordPicProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ChordPic = (props: IChordPicProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { chordPicList, match, loading } = props;
  return (
    <div>
      <h2 id="chord-pic-heading" data-cy="ChordPicHeading">
        <Translate contentKey="toneupApp.chordPic.home.title">Chord Pics</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.chordPic.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="toneupApp.chordPic.home.createLabel">Create new Chord Pic</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {chordPicList && chordPicList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="toneupApp.chordPic.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chordPic.pictureUrl">Picture Url</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chordPic.chordId">Chord Id</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chordPic.instrument">Instrument</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {chordPicList.map((chordPic, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${chordPic.id}`} color="link" size="sm">
                      {chordPic.id}
                    </Button>
                  </td>
                  <td>{chordPic.pictureUrl}</td>
                  <td>{chordPic.chordId ? <Link to={`chord/${chordPic.chordId.id}`}>{chordPic.chordId.id}</Link> : ''}</td>
                  <td>{chordPic.instrument ? <Link to={`instrument/${chordPic.instrument.id}`}>{chordPic.instrument.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${chordPic.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chordPic.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chordPic.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.chordPic.home.notFound">No Chord Pics found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ chordPic }: IRootState) => ({
  chordPicList: chordPic.entities,
  loading: chordPic.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChordPic);
