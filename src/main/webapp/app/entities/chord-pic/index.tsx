import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ChordPic from './chord-pic';
import ChordPicDetail from './chord-pic-detail';
import ChordPicUpdate from './chord-pic-update';
import ChordPicDeleteDialog from './chord-pic-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ChordPicUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ChordPicUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChordPicDetail} />
      <ErrorBoundaryRoute path={match.url} component={ChordPic} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ChordPicDeleteDialog} />
  </>
);

export default Routes;
