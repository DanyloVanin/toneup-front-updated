import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IChord } from 'app/shared/model/chord.model';
import { getEntities as getChords } from 'app/entities/chord/chord.reducer';
import { IInstrument } from 'app/shared/model/instrument.model';
import { getEntities as getInstruments } from 'app/entities/instrument/instrument.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chord-pic.reducer';
import { IChordPic } from 'app/shared/model/chord-pic.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChordPicUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChordPicUpdate = (props: IChordPicUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { chordPicEntity, chords, instruments, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/chord-pic');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getChords();
    props.getInstruments();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...chordPicEntity,
        ...values,
        chordId: chords.find(it => it.id.toString() === values.chordIdId.toString()),
        instrument: instruments.find(it => it.id.toString() === values.instrumentId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.chordPic.home.createOrEditLabel" data-cy="ChordPicCreateUpdateHeading">
            <Translate contentKey="toneupApp.chordPic.home.createOrEditLabel">Create or edit a ChordPic</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : chordPicEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="chord-pic-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="chord-pic-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="pictureUrlLabel" for="chord-pic-pictureUrl">
                  <Translate contentKey="toneupApp.chordPic.pictureUrl">Picture Url</Translate>
                </Label>
                <AvField
                  id="chord-pic-pictureUrl"
                  data-cy="pictureUrl"
                  type="text"
                  name="pictureUrl"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="chord-pic-chordId">
                  <Translate contentKey="toneupApp.chordPic.chordId">Chord Id</Translate>
                </Label>
                <AvInput id="chord-pic-chordId" data-cy="chordId" type="select" className="form-control" name="chordIdId" required>
                  <option value="" key="0" />
                  {chords
                    ? chords.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="chord-pic-instrument">
                  <Translate contentKey="toneupApp.chordPic.instrument">Instrument</Translate>
                </Label>
                <AvInput id="chord-pic-instrument" data-cy="instrument" type="select" className="form-control" name="instrumentId" required>
                  <option value="" key="0" />
                  {instruments
                    ? instruments.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/chord-pic" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  chords: storeState.chord.entities,
  instruments: storeState.instrument.entities,
  chordPicEntity: storeState.chordPic.entity,
  loading: storeState.chordPic.loading,
  updating: storeState.chordPic.updating,
  updateSuccess: storeState.chordPic.updateSuccess,
});

const mapDispatchToProps = {
  getChords,
  getInstruments,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChordPicUpdate);
