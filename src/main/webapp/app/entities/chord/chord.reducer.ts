import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IChord, defaultValue } from 'app/shared/model/chord.model';

export const ACTION_TYPES = {
  FETCH_CHORD_LIST: 'chord/FETCH_CHORD_LIST',
  FETCH_CHORD: 'chord/FETCH_CHORD',
  CREATE_CHORD: 'chord/CREATE_CHORD',
  UPDATE_CHORD: 'chord/UPDATE_CHORD',
  PARTIAL_UPDATE_CHORD: 'chord/PARTIAL_UPDATE_CHORD',
  DELETE_CHORD: 'chord/DELETE_CHORD',
  RESET: 'chord/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChord>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ChordState = Readonly<typeof initialState>;

// Reducer

export default (state: ChordState = initialState, action): ChordState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHORD_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CHORD):
    case REQUEST(ACTION_TYPES.UPDATE_CHORD):
    case REQUEST(ACTION_TYPES.DELETE_CHORD):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_CHORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CHORD_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHORD):
    case FAILURE(ACTION_TYPES.CREATE_CHORD):
    case FAILURE(ACTION_TYPES.UPDATE_CHORD):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_CHORD):
    case FAILURE(ACTION_TYPES.DELETE_CHORD):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHORD_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHORD):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHORD):
    case SUCCESS(ACTION_TYPES.UPDATE_CHORD):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_CHORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/chords';

// Actions

export const getEntities: ICrudGetAllAction<IChord> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CHORD_LIST,
  payload: axios.get<IChord>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IChord> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHORD,
    payload: axios.get<IChord>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IChord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHORD,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHORD,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IChord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_CHORD,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChord> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHORD,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
