import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './chord.reducer';
import { IChord } from 'app/shared/model/chord.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChordProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Chord = (props: IChordProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { chordList, match, loading } = props;
  return (
    <div>
      <h2 id="chord-heading" data-cy="ChordHeading">
        <Translate contentKey="toneupApp.chord.home.title">Chords</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.chord.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="toneupApp.chord.home.createLabel">Create new Chord</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {chordList && chordList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="toneupApp.chord.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chord.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chord.prevChord">Prev Chord</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.chord.nextChord">Next Chord</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {chordList.map((chord, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${chord.id}`} color="link" size="sm">
                      {chord.id}
                    </Button>
                  </td>
                  <td>{chord.name}</td>
                  <td>{chord.prevChord ? <Link to={`chord/${chord.prevChord.id}`}>{chord.prevChord.id}</Link> : ''}</td>
                  <td>{chord.nextChord ? <Link to={`chord/${chord.nextChord.id}`}>{chord.nextChord.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${chord.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chord.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${chord.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.chord.home.notFound">No Chords found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ chord }: IRootState) => ({
  chordList: chord.entities,
  loading: chord.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Chord);
