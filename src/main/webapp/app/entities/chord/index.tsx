import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Chord from './chord';
import ChordDetail from './chord-detail';
import ChordUpdate from './chord-update';
import ChordDeleteDialog from './chord-delete-dialog';
import PrivateRoute from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
// hasAnyAuthorities={[AUTHORITIES.ADMIN]}

const Routes = ({ match }) => (
  <>
    <Switch>
      <PrivateRoute exact path={`${match.url}/new`} component={ChordUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <PrivateRoute exact path={`${match.url}/:id/edit`} component={ChordUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChordDetail} />
      <ErrorBoundaryRoute path={match.url} component={Chord} />
    </Switch>
    <PrivateRoute exact path={`${match.url}/:id/delete`} component={ChordDeleteDialog} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
  </>
);

export default Routes;
