import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getChords } from 'app/entities/chord/chord.reducer';
import { ISongRecord } from 'app/shared/model/song-record.model';
import { getEntities as getSongRecords } from 'app/entities/song-record/song-record.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chord.reducer';
import { IChord } from 'app/shared/model/chord.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChordUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChordUpdate = (props: IChordUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { chordEntity, chords, songRecords, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/chord');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getChords();
    props.getSongRecords();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...chordEntity,
        ...values,
        prevChord: chords.find(it => it.id.toString() === values.prevChordId.toString()),
        nextChord: chords.find(it => it.id.toString() === values.nextChordId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.chord.home.createOrEditLabel" data-cy="ChordCreateUpdateHeading">
            <Translate contentKey="toneupApp.chord.home.createOrEditLabel">Create or edit a Chord</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : chordEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="chord-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="chord-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="chord-name">
                  <Translate contentKey="toneupApp.chord.name">Name</Translate>
                </Label>
                <AvField
                  id="chord-name"
                  data-cy="name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="chord-prevChord">
                  <Translate contentKey="toneupApp.chord.prevChord">Prev Chord</Translate>
                </Label>
                <AvInput id="chord-prevChord" data-cy="prevChord" type="select" className="form-control" name="prevChordId">
                  <option value="" key="0" />
                  {chords
                    ? chords.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="chord-nextChord">
                  <Translate contentKey="toneupApp.chord.nextChord">Next Chord</Translate>
                </Label>
                <AvInput id="chord-nextChord" data-cy="nextChord" type="select" className="form-control" name="nextChordId">
                  <option value="" key="0" />
                  {chords
                    ? chords.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/chord" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  chords: storeState.chord.entities,
  songRecords: storeState.songRecord.entities,
  chordEntity: storeState.chord.entity,
  loading: storeState.chord.loading,
  updating: storeState.chord.updating,
  updateSuccess: storeState.chord.updateSuccess,
});

const mapDispatchToProps = {
  getChords,
  getSongRecords,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChordUpdate);
