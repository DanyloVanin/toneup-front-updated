import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGenre } from 'app/shared/model/genre.model';
import { getEntities as getGenres } from 'app/entities/genre/genre.reducer';
import { IArtist } from 'app/shared/model/artist.model';
import { getEntities as getArtists } from 'app/entities/artist/artist.reducer';
import { IInstrument } from 'app/shared/model/instrument.model';
import { getEntities as getInstruments } from 'app/entities/instrument/instrument.reducer';
import { getEntity, updateEntity, createEntity, reset } from './song.reducer';
import { ISong } from 'app/shared/model/song.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISongUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongUpdate = (props: ISongUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { songEntity, genres, artists, instruments, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/song' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getGenres();
    props.getArtists();
    props.getInstruments();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.dateAdded = convertDateTimeToServer(values.dateAdded);

    if (errors.length === 0) {
      const entity = {
        ...songEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.song.home.createOrEditLabel" data-cy="SongCreateUpdateHeading">
            <Translate contentKey="toneupApp.song.home.createOrEditLabel">Create or edit a Song</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : songEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="song-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="song-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="song-name">
                  <Translate contentKey="toneupApp.song.name">Name</Translate>
                </Label>
                <AvField
                  id="song-name"
                  data-cy="name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="languageLabel" for="song-language">
                  <Translate contentKey="toneupApp.song.language">Language</Translate>
                </Label>
                <AvField id="song-language" data-cy="language" type="text" name="language" />
              </AvGroup>
              <AvGroup>
                <Label id="dateAddedLabel" for="song-dateAdded">
                  <Translate contentKey="toneupApp.song.dateAdded">Date Added</Translate>
                </Label>
                <AvInput
                  id="song-dateAdded"
                  data-cy="dateAdded"
                  type="datetime-local"
                  className="form-control"
                  name="dateAdded"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.songEntity.dateAdded)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="spotifyUrlLabel" for="song-spotifyUrl">
                  <Translate contentKey="toneupApp.song.spotifyUrl">Spotify Url</Translate>
                </Label>
                <AvField id="song-spotifyUrl" data-cy="spotifyUrl" type="text" name="spotifyUrl" />
              </AvGroup>
              <AvGroup>
                <Label id="youtubeUrlLabel" for="song-youtubeUrl">
                  <Translate contentKey="toneupApp.song.youtubeUrl">Youtube Url</Translate>
                </Label>
                <AvField id="song-youtubeUrl" data-cy="youtubeUrl" type="text" name="youtubeUrl" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/song" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  genres: storeState.genre.entities,
  artists: storeState.artist.entities,
  instruments: storeState.instrument.entities,
  songEntity: storeState.song.entity,
  loading: storeState.song.loading,
  updating: storeState.song.updating,
  updateSuccess: storeState.song.updateSuccess,
});

const mapDispatchToProps = {
  getGenres,
  getArtists,
  getInstruments,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongUpdate);
