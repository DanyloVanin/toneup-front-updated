import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './song.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISongDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongDetail = (props: ISongDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { songEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="songDetailsHeading">
          <Translate contentKey="toneupApp.song.detail.title">Song</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{songEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="toneupApp.song.name">Name</Translate>
            </span>
          </dt>
          <dd>{songEntity.name}</dd>
          <dt>
            <span id="language">
              <Translate contentKey="toneupApp.song.language">Language</Translate>
            </span>
          </dt>
          <dd>{songEntity.language}</dd>
          <dt>
            <span id="dateAdded">
              <Translate contentKey="toneupApp.song.dateAdded">Date Added</Translate>
            </span>
          </dt>
          <dd>{songEntity.dateAdded ? <TextFormat value={songEntity.dateAdded} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="spotifyUrl">
              <Translate contentKey="toneupApp.song.spotifyUrl">Spotify Url</Translate>
            </span>
          </dt>
          <dd>{songEntity.spotifyUrl}</dd>
          <dt>
            <span id="youtubeUrl">
              <Translate contentKey="toneupApp.song.youtubeUrl">Youtube Url</Translate>
            </span>
          </dt>
          <dd>{songEntity.youtubeUrl}</dd>
        </dl>
        <Button tag={Link} to="/song" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/song/${songEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ song }: IRootState) => ({
  songEntity: song.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongDetail);
