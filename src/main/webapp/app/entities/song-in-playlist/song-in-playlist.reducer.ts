import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISongInPlaylist, defaultValue } from 'app/shared/model/song-in-playlist.model';

export const ACTION_TYPES = {
  FETCH_SONGINPLAYLIST_LIST: 'songInPlaylist/FETCH_SONGINPLAYLIST_LIST',
  FETCH_SONGINPLAYLIST: 'songInPlaylist/FETCH_SONGINPLAYLIST',
  CREATE_SONGINPLAYLIST: 'songInPlaylist/CREATE_SONGINPLAYLIST',
  UPDATE_SONGINPLAYLIST: 'songInPlaylist/UPDATE_SONGINPLAYLIST',
  PARTIAL_UPDATE_SONGINPLAYLIST: 'songInPlaylist/PARTIAL_UPDATE_SONGINPLAYLIST',
  DELETE_SONGINPLAYLIST: 'songInPlaylist/DELETE_SONGINPLAYLIST',
  RESET: 'songInPlaylist/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISongInPlaylist>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type SongInPlaylistState = Readonly<typeof initialState>;

// Reducer

export default (state: SongInPlaylistState = initialState, action): SongInPlaylistState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SONGINPLAYLIST_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SONGINPLAYLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_SONGINPLAYLIST):
    case REQUEST(ACTION_TYPES.UPDATE_SONGINPLAYLIST):
    case REQUEST(ACTION_TYPES.DELETE_SONGINPLAYLIST):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_SONGINPLAYLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_SONGINPLAYLIST_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SONGINPLAYLIST):
    case FAILURE(ACTION_TYPES.CREATE_SONGINPLAYLIST):
    case FAILURE(ACTION_TYPES.UPDATE_SONGINPLAYLIST):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_SONGINPLAYLIST):
    case FAILURE(ACTION_TYPES.DELETE_SONGINPLAYLIST):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_SONGINPLAYLIST_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_SONGINPLAYLIST):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_SONGINPLAYLIST):
    case SUCCESS(ACTION_TYPES.UPDATE_SONGINPLAYLIST):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_SONGINPLAYLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_SONGINPLAYLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/song-in-playlists';

// Actions

export const getEntities: ICrudGetAllAction<ISongInPlaylist> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SONGINPLAYLIST_LIST,
  payload: axios.get<ISongInPlaylist>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ISongInPlaylist> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SONGINPLAYLIST,
    payload: axios.get<ISongInPlaylist>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ISongInPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SONGINPLAYLIST,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISongInPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SONGINPLAYLIST,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<ISongInPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_SONGINPLAYLIST,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISongInPlaylist> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SONGINPLAYLIST,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
