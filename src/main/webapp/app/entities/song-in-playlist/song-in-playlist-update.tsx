import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISongRecord } from 'app/shared/model/song-record.model';
import { getEntities as getSongRecords } from 'app/entities/song-record/song-record.reducer';
import { IUserPlaylist } from 'app/shared/model/user-playlist.model';
import { getEntities as getUserPlaylists } from 'app/entities/user-playlist/user-playlist.reducer';
import { getEntity, updateEntity, createEntity, reset } from './song-in-playlist.reducer';
import { ISongInPlaylist } from 'app/shared/model/song-in-playlist.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISongInPlaylistUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongInPlaylistUpdate = (props: ISongInPlaylistUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { songInPlaylistEntity, songRecords, userPlaylists, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/song-in-playlist');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getSongRecords();
    props.getUserPlaylists();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.dateAdded = convertDateTimeToServer(values.dateAdded);

    if (errors.length === 0) {
      const entity = {
        ...songInPlaylistEntity,
        ...values,
        songRecordId: songRecords.find(it => it.id.toString() === values.songRecordIdId.toString()),
        userPlaylistId: userPlaylists.find(it => it.id.toString() === values.userPlaylistIdId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.songInPlaylist.home.createOrEditLabel" data-cy="SongInPlaylistCreateUpdateHeading">
            <Translate contentKey="toneupApp.songInPlaylist.home.createOrEditLabel">Create or edit a SongInPlaylist</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : songInPlaylistEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="song-in-playlist-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="song-in-playlist-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="dateAddedLabel" for="song-in-playlist-dateAdded">
                  <Translate contentKey="toneupApp.songInPlaylist.dateAdded">Date Added</Translate>
                </Label>
                <AvInput
                  id="song-in-playlist-dateAdded"
                  data-cy="dateAdded"
                  type="datetime-local"
                  className="form-control"
                  name="dateAdded"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.songInPlaylistEntity.dateAdded)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="song-in-playlist-songRecordId">
                  <Translate contentKey="toneupApp.songInPlaylist.songRecordId">Song Record Id</Translate>
                </Label>
                <AvInput
                  id="song-in-playlist-songRecordId"
                  data-cy="songRecordId"
                  type="select"
                  className="form-control"
                  name="songRecordIdId"
                  required
                >
                  <option value="" key="0" />
                  {songRecords
                    ? songRecords.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="song-in-playlist-userPlaylistId">
                  <Translate contentKey="toneupApp.songInPlaylist.userPlaylistId">User Playlist Id</Translate>
                </Label>
                <AvInput
                  id="song-in-playlist-userPlaylistId"
                  data-cy="userPlaylistId"
                  type="select"
                  className="form-control"
                  name="userPlaylistIdId"
                  required
                >
                  <option value="" key="0" />
                  {userPlaylists
                    ? userPlaylists.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/song-in-playlist" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  songRecords: storeState.songRecord.entities,
  userPlaylists: storeState.userPlaylist.entities,
  songInPlaylistEntity: storeState.songInPlaylist.entity,
  loading: storeState.songInPlaylist.loading,
  updating: storeState.songInPlaylist.updating,
  updateSuccess: storeState.songInPlaylist.updateSuccess,
});

const mapDispatchToProps = {
  getSongRecords,
  getUserPlaylists,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongInPlaylistUpdate);
