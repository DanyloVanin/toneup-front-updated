import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './song-in-playlist.reducer';
import { ISongInPlaylist } from 'app/shared/model/song-in-playlist.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISongInPlaylistProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const SongInPlaylist = (props: ISongInPlaylistProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { songInPlaylistList, match, loading } = props;
  return (
    <div>
      <h2 id="song-in-playlist-heading" data-cy="SongInPlaylistHeading">
        <Translate contentKey="toneupApp.songInPlaylist.home.title">Song In Playlists</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.songInPlaylist.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="toneupApp.songInPlaylist.home.createLabel">Create new Song In Playlist</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {songInPlaylistList && songInPlaylistList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="toneupApp.songInPlaylist.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.songInPlaylist.dateAdded">Date Added</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.songInPlaylist.songRecordId">Song Record Id</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.songInPlaylist.userPlaylistId">User Playlist Id</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {songInPlaylistList.map((songInPlaylist, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${songInPlaylist.id}`} color="link" size="sm">
                      {songInPlaylist.id}
                    </Button>
                  </td>
                  <td>
                    {songInPlaylist.dateAdded ? <TextFormat type="date" value={songInPlaylist.dateAdded} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {songInPlaylist.songRecordId ? (
                      <Link to={`song-record/${songInPlaylist.songRecordId.id}`}>{songInPlaylist.songRecordId.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {songInPlaylist.userPlaylistId ? (
                      <Link to={`user-playlist/${songInPlaylist.userPlaylistId.id}`}>{songInPlaylist.userPlaylistId.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${songInPlaylist.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${songInPlaylist.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${songInPlaylist.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.songInPlaylist.home.notFound">No Song In Playlists found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ songInPlaylist }: IRootState) => ({
  songInPlaylistList: songInPlaylist.entities,
  loading: songInPlaylist.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongInPlaylist);
