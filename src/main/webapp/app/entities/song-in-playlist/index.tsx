import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SongInPlaylist from './song-in-playlist';
import SongInPlaylistDetail from './song-in-playlist-detail';
import SongInPlaylistUpdate from './song-in-playlist-update';
import SongInPlaylistDeleteDialog from './song-in-playlist-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SongInPlaylistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SongInPlaylistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SongInPlaylistDetail} />
      <ErrorBoundaryRoute path={match.url} component={SongInPlaylist} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={SongInPlaylistDeleteDialog} />
  </>
);

export default Routes;
