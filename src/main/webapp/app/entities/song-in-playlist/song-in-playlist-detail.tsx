import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './song-in-playlist.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISongInPlaylistDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongInPlaylistDetail = (props: ISongInPlaylistDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { songInPlaylistEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="songInPlaylistDetailsHeading">
          <Translate contentKey="toneupApp.songInPlaylist.detail.title">SongInPlaylist</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{songInPlaylistEntity.id}</dd>
          <dt>
            <span id="dateAdded">
              <Translate contentKey="toneupApp.songInPlaylist.dateAdded">Date Added</Translate>
            </span>
          </dt>
          <dd>
            {songInPlaylistEntity.dateAdded ? (
              <TextFormat value={songInPlaylistEntity.dateAdded} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="toneupApp.songInPlaylist.songRecordId">Song Record Id</Translate>
          </dt>
          <dd>{songInPlaylistEntity.songRecordId ? songInPlaylistEntity.songRecordId.id : ''}</dd>
          <dt>
            <Translate contentKey="toneupApp.songInPlaylist.userPlaylistId">User Playlist Id</Translate>
          </dt>
          <dd>{songInPlaylistEntity.userPlaylistId ? songInPlaylistEntity.userPlaylistId.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/song-in-playlist" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/song-in-playlist/${songInPlaylistEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ songInPlaylist }: IRootState) => ({
  songInPlaylistEntity: songInPlaylist.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongInPlaylistDetail);
