import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserPlaylist from './user-playlist';
import UserPlaylistDetail from './user-playlist-detail';
import UserPlaylistUpdate from './user-playlist-update';
import UserPlaylistDeleteDialog from './user-playlist-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserPlaylistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserPlaylistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserPlaylistDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserPlaylist} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={UserPlaylistDeleteDialog} />
  </>
);

export default Routes;
