import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-playlist.reducer';
import { IUserPlaylist } from 'app/shared/model/user-playlist.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserPlaylistProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const UserPlaylist = (props: IUserPlaylistProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { userPlaylistList, match, loading } = props;
  return (
    <div>
      <h2 id="user-playlist-heading" data-cy="UserPlaylistHeading">
        <Translate contentKey="toneupApp.userPlaylist.home.title">User Playlists</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.userPlaylist.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="toneupApp.userPlaylist.home.createLabel">Create new User Playlist</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {userPlaylistList && userPlaylistList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="toneupApp.userPlaylist.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.userPlaylist.playlistName">Playlist Name</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.userPlaylist.dateCreated">Date Created</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.userPlaylist.userId">User Id</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {userPlaylistList.map((userPlaylist, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${userPlaylist.id}`} color="link" size="sm">
                      {userPlaylist.id}
                    </Button>
                  </td>
                  <td>{userPlaylist.playlistName}</td>
                  <td>
                    {userPlaylist.dateCreated ? <TextFormat type="date" value={userPlaylist.dateCreated} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>{userPlaylist.user ? <Link to={`user-info/${userPlaylist.user.id}`}>{userPlaylist.user.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${userPlaylist.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userPlaylist.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${userPlaylist.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.userPlaylist.home.notFound">No User Playlists found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ userPlaylist }: IRootState) => ({
  userPlaylistList: userPlaylist.entities,
  loading: userPlaylist.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserPlaylist);
