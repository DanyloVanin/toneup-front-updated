import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './user-playlist.reducer';
import { IUserPlaylist } from 'app/shared/model/user-playlist.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserPlaylistUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserPlaylistUpdate = (props: IUserPlaylistUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { userPlaylistEntity, userInfos, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/user-playlist');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
    
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.dateCreated = convertDateTimeToServer(values.dateCreated);

    if (errors.length === 0) {
      const entity = {
        ...userPlaylistEntity,
        ...values,
        userId: userInfos.find(it => it.id.toString() === values.userIdId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.userPlaylist.home.createOrEditLabel" data-cy="UserPlaylistCreateUpdateHeading">
            <Translate contentKey="toneupApp.userPlaylist.home.createOrEditLabel">Create or edit a UserPlaylist</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : userPlaylistEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="user-playlist-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="user-playlist-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="playlistNameLabel" for="user-playlist-playlistName">
                  <Translate contentKey="toneupApp.userPlaylist.playlistName">Playlist Name</Translate>
                </Label>
                <AvField
                  id="user-playlist-playlistName"
                  data-cy="playlistName"
                  type="text"
                  name="playlistName"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="dateCreatedLabel" for="user-playlist-dateCreated">
                  <Translate contentKey="toneupApp.userPlaylist.dateCreated">Date Created</Translate>
                </Label>
                <AvInput
                  id="user-playlist-dateCreated"
                  data-cy="dateCreated"
                  type="datetime-local"
                  className="form-control"
                  name="dateCreated"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.userPlaylistEntity.dateCreated)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="user-playlist-userId">
                  <Translate contentKey="toneupApp.userPlaylist.userId">User Id</Translate>
                </Label>
                <AvInput id="user-playlist-userId" data-cy="userId" type="select" className="form-control" name="userIdId" required>
                  <option value="" key="0" />
                  {userInfos
                    ? userInfos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/user-playlist" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  userInfos: storeState.userManagement.users,
  userPlaylistEntity: storeState.userPlaylist.entity,
  loading: storeState.userPlaylist.loading,
  updating: storeState.userPlaylist.updating,
  updateSuccess: storeState.userPlaylist.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserPlaylistUpdate);
