import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-playlist.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserPlaylistDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserPlaylistDetail = (props: IUserPlaylistDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { userPlaylistEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="userPlaylistDetailsHeading">
          <Translate contentKey="toneupApp.userPlaylist.detail.title">UserPlaylist</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{userPlaylistEntity.id}</dd>
          <dt>
            <span id="playlistName">
              <Translate contentKey="toneupApp.userPlaylist.playlistName">Playlist Name</Translate>
            </span>
          </dt>
          <dd>{userPlaylistEntity.playlistName}</dd>
          <dt>
            <span id="dateCreated">
              <Translate contentKey="toneupApp.userPlaylist.dateCreated">Date Created</Translate>
            </span>
          </dt>
          <dd>
            {userPlaylistEntity.dateCreated ? (
              <TextFormat value={userPlaylistEntity.dateCreated} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="toneupApp.userPlaylist.userId">User Id</Translate>
          </dt>
          <dd>{userPlaylistEntity.user ? userPlaylistEntity.user.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/user-playlist" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/user-playlist/${userPlaylistEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ userPlaylist }: IRootState) => ({
  userPlaylistEntity: userPlaylist.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserPlaylistDetail);
