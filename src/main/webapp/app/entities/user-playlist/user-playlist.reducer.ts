import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUserPlaylist, defaultValue } from 'app/shared/model/user-playlist.model';

export const ACTION_TYPES = {
  FETCH_USERPLAYLIST_LIST: 'userPlaylist/FETCH_USERPLAYLIST_LIST',
  FETCH_USERPLAYLIST: 'userPlaylist/FETCH_USERPLAYLIST',
  CREATE_USERPLAYLIST: 'userPlaylist/CREATE_USERPLAYLIST',
  UPDATE_USERPLAYLIST: 'userPlaylist/UPDATE_USERPLAYLIST',
  PARTIAL_UPDATE_USERPLAYLIST: 'userPlaylist/PARTIAL_UPDATE_USERPLAYLIST',
  DELETE_USERPLAYLIST: 'userPlaylist/DELETE_USERPLAYLIST',
  RESET: 'userPlaylist/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserPlaylist>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type UserPlaylistState = Readonly<typeof initialState>;

// Reducer

export default (state: UserPlaylistState = initialState, action): UserPlaylistState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USERPLAYLIST_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USERPLAYLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_USERPLAYLIST):
    case REQUEST(ACTION_TYPES.UPDATE_USERPLAYLIST):
    case REQUEST(ACTION_TYPES.DELETE_USERPLAYLIST):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_USERPLAYLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_USERPLAYLIST_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USERPLAYLIST):
    case FAILURE(ACTION_TYPES.CREATE_USERPLAYLIST):
    case FAILURE(ACTION_TYPES.UPDATE_USERPLAYLIST):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_USERPLAYLIST):
    case FAILURE(ACTION_TYPES.DELETE_USERPLAYLIST):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERPLAYLIST_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERPLAYLIST):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_USERPLAYLIST):
    case SUCCESS(ACTION_TYPES.UPDATE_USERPLAYLIST):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_USERPLAYLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_USERPLAYLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/user-playlists';

// Actions

export const getEntities: ICrudGetAllAction<IUserPlaylist> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_USERPLAYLIST_LIST,
  payload: axios.get<IUserPlaylist>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IUserPlaylist> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USERPLAYLIST,
    payload: axios.get<IUserPlaylist>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IUserPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USERPLAYLIST,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERPLAYLIST,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IUserPlaylist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_USERPLAYLIST,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserPlaylist> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USERPLAYLIST,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
