import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SongRecord from './song-record';
import SongRecordDetail from './song-record-detail';
import SongRecordUpdate from './song-record-update';
import SongRecordDeleteDialog from './song-record-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SongRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SongRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SongRecordDetail} />
      <ErrorBoundaryRoute path={match.url} component={SongRecord} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={SongRecordDeleteDialog} />
  </>
);

export default Routes;
