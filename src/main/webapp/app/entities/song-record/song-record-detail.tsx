import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './song-record.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISongRecordDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongRecordDetail = (props: ISongRecordDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { songRecordEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="songRecordDetailsHeading">
          <Translate contentKey="toneupApp.songRecord.detail.title">SongRecord</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.id}</dd>
          <dt>
            <span id="tonality">
              <Translate contentKey="toneupApp.songRecord.tonality">Tonality</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.tonality}</dd>
          <dt>
            <span id="lyrics">
              <Translate contentKey="toneupApp.songRecord.lyrics">Lyrics</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.lyrics}</dd>
          <dt>
            <span id="tempo">
              <Translate contentKey="toneupApp.songRecord.tempo">Tempo</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.tempo}</dd>
          <dt>
            <span id="rating">
              <Translate contentKey="toneupApp.songRecord.rating">Rating</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.rating}</dd>
          <dt>
            <span id="dateUploaded">
              <Translate contentKey="toneupApp.songRecord.dateUploaded">Date Uploaded</Translate>
            </span>
          </dt>
          <dd>
            {songRecordEntity.dateUploaded ? (
              <TextFormat value={songRecordEntity.dateUploaded} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="difficulty">
              <Translate contentKey="toneupApp.songRecord.difficulty">Difficulty</Translate>
            </span>
          </dt>
          <dd>{songRecordEntity.difficulty}</dd>
          <dt>
            <Translate contentKey="toneupApp.songRecord.authorId">Author Id</Translate>
          </dt>
          <dd>{songRecordEntity.author ? songRecordEntity.author.id : ''}</dd>
          <dt>
            <Translate contentKey="toneupApp.songRecord.songId">Song Id</Translate>
          </dt>
          <dd>{songRecordEntity.song ? songRecordEntity.song.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/song-record" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/song-record/${songRecordEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ songRecord }: IRootState) => ({
  songRecordEntity: songRecord.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongRecordDetail);
