import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { setFileData, byteSize, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IChord } from 'app/shared/model/chord.model';
import { getEntities as getChords } from 'app/entities/chord/chord.reducer';
import { ISong } from 'app/shared/model/song.model';
import { getEntities as getSongs } from 'app/entities/song/song.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './song-record.reducer';
import { ISongRecord } from 'app/shared/model/song-record.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISongRecordUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SongRecordUpdate = (props: ISongRecordUpdateProps) => {
  const [idschordId, setIdschordId] = useState([]);
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { songRecordEntity, users, chords, songs, loading, updating } = props;

  const { lyrics } = songRecordEntity;

  const handleClose = () => {
    props.history.push('/song-record' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUsers();
    props.getChords();
    props.getSongs();
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.dateUploaded = convertDateTimeToServer(values.dateUploaded);

    if (errors.length === 0) {
      const entity = {
        ...songRecordEntity,
        ...values,
        chordIds: mapIdList(values.chordIds),
        authorId: users.find(it => it.id.toString() === values.authorIdId.toString()),
        songId: songs.find(it => it.id.toString() === values.songIdId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.songRecord.home.createOrEditLabel" data-cy="SongRecordCreateUpdateHeading">
            <Translate contentKey="toneupApp.songRecord.home.createOrEditLabel">Create or edit a SongRecord</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : songRecordEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="song-record-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="song-record-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="tonalityLabel" for="song-record-tonality">
                  <Translate contentKey="toneupApp.songRecord.tonality">Tonality</Translate>
                </Label>
                <AvField
                  id="song-record-tonality"
                  data-cy="tonality"
                  type="text"
                  name="tonality"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="lyricsLabel" for="song-record-lyrics">
                  <Translate contentKey="toneupApp.songRecord.lyrics">Lyrics</Translate>
                </Label>
                <AvInput
                  id="song-record-lyrics"
                  data-cy="lyrics"
                  type="textarea"
                  name="lyrics"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="tempoLabel" for="song-record-tempo">
                  <Translate contentKey="toneupApp.songRecord.tempo">Tempo</Translate>
                </Label>
                <AvField
                  id="song-record-tempo"
                  data-cy="tempo"
                  type="string"
                  className="form-control"
                  name="tempo"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="ratingLabel" for="song-record-rating">
                  <Translate contentKey="toneupApp.songRecord.rating">Rating</Translate>
                </Label>
                <AvField id="song-record-rating" data-cy="rating" type="string" className="form-control" name="rating" />
              </AvGroup>
              <AvGroup>
                <Label id="dateUploadedLabel" for="song-record-dateUploaded">
                  <Translate contentKey="toneupApp.songRecord.dateUploaded">Date Uploaded</Translate>
                </Label>
                <AvInput
                  id="song-record-dateUploaded"
                  data-cy="dateUploaded"
                  type="datetime-local"
                  className="form-control"
                  name="dateUploaded"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.songRecordEntity.dateUploaded)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="difficultyLabel" for="song-record-difficulty">
                  <Translate contentKey="toneupApp.songRecord.difficulty">Difficulty</Translate>
                </Label>
                <AvInput
                  id="song-record-difficulty"
                  data-cy="difficulty"
                  type="select"
                  className="form-control"
                  name="difficulty"
                  value={(!isNew && songRecordEntity.difficulty) || 'EASY'}
                >
                  <option value="EASY">{translate('toneupApp.Difficulty.EASY')}</option>
                  <option value="MEDIUM">{translate('toneupApp.Difficulty.MEDIUM')}</option>
                  <option value="HARD">{translate('toneupApp.Difficulty.HARD')}</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="song-record-authorId">
                  <Translate contentKey="toneupApp.songRecord.authorId">Author Id</Translate>
                </Label>
                <AvInput id="song-record-authorId" data-cy="authorId" type="select" className="form-control" name="authorIdId" required>
                  <option value="" key="0" />
                  {users
                    ? users.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="song-record-chordId">
                  <Translate contentKey="toneupApp.songRecord.chordId">Chord Id</Translate>
                </Label>
                <AvInput
                  id="song-record-chordId"
                  data-cy="chordId"
                  type="select"
                  multiple
                  className="form-control"
                  name="chordIds"
                  value={!isNew && songRecordEntity.chords && songRecordEntity.chords.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {chords
                    ? chords.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="song-record-songId">
                  <Translate contentKey="toneupApp.songRecord.songId">Song Id</Translate>
                </Label>
                <AvInput id="song-record-songId" data-cy="songId" type="select" className="form-control" name="songIdId" required>
                  <option value="" key="0" />
                  {songs
                    ? songs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/song-record" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  chords: storeState.chord.entities,
  songs: storeState.song.entities,
  songRecordEntity: storeState.songRecord.entity,
  loading: storeState.songRecord.loading,
  updating: storeState.songRecord.updating,
  updateSuccess: storeState.songRecord.updateSuccess,
});

const mapDispatchToProps = {
  getUsers,
  getChords,
  getSongs,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SongRecordUpdate);
