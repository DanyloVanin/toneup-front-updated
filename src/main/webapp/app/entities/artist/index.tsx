import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Artist from './artist';
import ArtistDetail from './artist-detail';
import ArtistUpdate from './artist-update';
import ArtistDeleteDialog from './artist-delete-dialog';
import PrivateRoute from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
// hasAnyAuthorities={[AUTHORITIES.ADMIN]}

const Routes = ({ match }) => (
  <>
    <Switch>
      <PrivateRoute exact path={`${match.url}/new`} component={ArtistUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
      <PrivateRoute exact path={`${match.url}/:id/edit`} component={ArtistUpdate} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ArtistDetail} />
      <ErrorBoundaryRoute path={match.url} component={Artist} />
    </Switch>
    <PrivateRoute exact path={`${match.url}/:id/delete`} component={ArtistDeleteDialog} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
  </>
);

export default Routes;
