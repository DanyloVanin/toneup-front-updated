import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './artist.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IArtistDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ArtistDetail = (props: IArtistDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { artistEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="artistDetailsHeading">
          <Translate contentKey="toneupApp.artist.detail.title">Artist</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{artistEntity.id}</dd>
          <dt>
            <span id="artistName">
              <Translate contentKey="toneupApp.artist.artistName">Artist Name</Translate>
            </span>
          </dt>
          <dd>{artistEntity.artistName}</dd>
          <dt>
            <span id="bio">
              <Translate contentKey="toneupApp.artist.bio">Bio</Translate>
            </span>
          </dt>
          <dd>{artistEntity.bio}</dd>
          <dt>
            <span id="artistPicUrl">
              <Translate contentKey="toneupApp.artist.artistPicUrl">Artist Pic Url</Translate>
            </span>
          </dt>
          <dd>{artistEntity.artistPicUrl}</dd>
        </dl>
        <Button tag={Link} to="/artist" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/artist/${artistEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ artist }: IRootState) => ({
  artistEntity: artist.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ArtistDetail);
