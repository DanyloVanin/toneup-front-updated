import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './artist.reducer';
import { IArtist } from 'app/shared/model/artist.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IArtistProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Artist = (props: IArtistProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { artistList, match, loading } = props;
  return (
    <div>
      <h2 id="artist-heading" data-cy="ArtistHeading">
        <Translate contentKey="toneupApp.artist.home.title">Artists</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="toneupApp.artist.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="toneupApp.artist.home.createLabel">Create new Artist</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {artistList && artistList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="toneupApp.artist.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.artist.artistName">Artist Name</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.artist.bio">Bio</Translate>
                </th>
                <th>
                  <Translate contentKey="toneupApp.artist.artistPicUrl">Artist Pic Url</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {artistList.map((artist, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${artist.id}`} color="link" size="sm">
                      {artist.id}
                    </Button>
                  </td>
                  <td>{artist.artistName}</td>
                  <td>{artist.bio}</td>
                  <td>{artist.artistPicUrl}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${artist.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${artist.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${artist.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="toneupApp.artist.home.notFound">No Artists found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ artist }: IRootState) => ({
  artistList: artist.entities,
  loading: artist.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Artist);
