import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISong } from 'app/shared/model/song.model';
import { getEntities as getSongs } from 'app/entities/song/song.reducer';
import { getEntity, updateEntity, createEntity, reset } from './instrument.reducer';
import { IInstrument } from 'app/shared/model/instrument.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IInstrumentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const InstrumentUpdate = (props: IInstrumentUpdateProps) => {
  const [idssongId, setIdssongId] = useState([]);
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { instrumentEntity, songs, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/instrument');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getSongs();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...instrumentEntity,
        ...values,
        songIds: mapIdList(values.songIds),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="toneupApp.instrument.home.createOrEditLabel" data-cy="InstrumentCreateUpdateHeading">
            <Translate contentKey="toneupApp.instrument.home.createOrEditLabel">Create or edit a Instrument</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : instrumentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="instrument-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="instrument-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="instrument-name">
                  <Translate contentKey="toneupApp.instrument.name">Name</Translate>
                </Label>
                <AvField
                  id="instrument-name"
                  data-cy="name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="instrument-songId">
                  <Translate contentKey="toneupApp.instrument.songId">Song Id</Translate>
                </Label>
                <AvInput
                  id="instrument-songId"
                  data-cy="songId"
                  type="select"
                  multiple
                  className="form-control"
                  name="songIds"
                  value={!isNew && instrumentEntity.songIds && instrumentEntity.songIds.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {songs
                    ? songs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/instrument" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  songs: storeState.song.entities,
  instrumentEntity: storeState.instrument.entity,
  loading: storeState.instrument.loading,
  updating: storeState.instrument.updating,
  updateSuccess: storeState.instrument.updateSuccess,
});

const mapDispatchToProps = {
  getSongs,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(InstrumentUpdate);
