import dayjs from 'dayjs';
import { IGenre } from 'app/shared/model/genre.model';
import { IArtist } from 'app/shared/model/artist.model';

export interface IAlbum {
  id?: number;
  albumName?: string;
  releaseYear?: string | null;
  description?: string | null;
  albumPicUrl?: string | null;
  genreIds?: IGenre[] | null;
  artistIds?: IArtist[] | null;
}

export const defaultValue: Readonly<IAlbum> = {};
