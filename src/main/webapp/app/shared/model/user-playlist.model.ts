import dayjs from 'dayjs';
import { ISongInPlaylist } from 'app/shared/model/song-in-playlist.model';
import { IUser } from 'app/shared/model/user.model';

export interface IUserPlaylist {
  id?: number;
  playlistName?: string;
  dateCreated?: string;
  songPlaylists?: ISongInPlaylist[] | null;
  user?: IUser;
}

export const defaultValue: Readonly<IUserPlaylist> = {};
