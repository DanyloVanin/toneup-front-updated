import { IChordPic } from 'app/shared/model/chord-pic.model';
import { ISongRecord } from 'app/shared/model/song-record.model';

export interface IChord {
  id?: number;
  name?: string;
  prevChord?: IChord | null;
  nextChord?: IChord | null;
  chordPics?: IChordPic[] | null;
  songRecordIds?: ISongRecord[];
}

export const defaultValue: Readonly<IChord> = {};
