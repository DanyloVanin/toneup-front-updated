import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';
import { ISongInPlaylist } from 'app/shared/model/song-in-playlist.model';
import { IComment } from 'app/shared/model/comment.model';
import { IChord } from 'app/shared/model/chord.model';
import { ISong } from 'app/shared/model/song.model';
import { Difficulty } from 'app/shared/model/enumerations/difficulty.model';

export interface ISongRecord {
  id?: number;
  tonality?: string;
  lyrics?: string;
  tempo?: number;
  rating?: number | null;
  dateUploaded?: string;
  difficulty?: Difficulty | null;
  author?: IUser;
  songInPlaylists?: ISongInPlaylist[] | null;
  comments?: IComment[] | null;
  chords?: IChord[] | null;
  song?: ISong;
}

export const defaultValue: Readonly<ISongRecord> = {};
