import dayjs from 'dayjs';
import { ISongRecord } from 'app/shared/model/song-record.model';
import { IUserPlaylist } from 'app/shared/model/user-playlist.model';

export interface ISongInPlaylist {
  id?: number;
  dateAdded?: string;
  songRecordId?: ISongRecord;
  userPlaylistId?: IUserPlaylist;
}

export const defaultValue: Readonly<ISongInPlaylist> = {};
