import { ISong } from 'app/shared/model/song.model';
import { IAlbum } from 'app/shared/model/album.model';

export interface IGenre {
  id?: number;
  name?: string;
  songIds?: ISong[] | null;
  albumIds?: IAlbum[] | null;
}

export const defaultValue: Readonly<IGenre> = {};
