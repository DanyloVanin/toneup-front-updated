import dayjs from 'dayjs';
import { ISongRecord } from 'app/shared/model/song-record.model';
import { IGenre } from 'app/shared/model/genre.model';
import { IArtist } from 'app/shared/model/artist.model';
import { IInstrument } from 'app/shared/model/instrument.model';

export interface ISong {
  id?: number;
  name?: string;
  language?: string | null;
  dateAdded?: string | null;
  spotifyUrl?: string | null;
  youtubeUrl?: string | null;
  songRecords?: ISongRecord[] | null;
  genres?: IGenre[] | null;
  artists?: IArtist[] | null;
  instruments?: IInstrument[] | null;
}

export const defaultValue: Readonly<ISong> = {};
