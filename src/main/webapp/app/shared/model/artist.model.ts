import { IAlbum } from 'app/shared/model/album.model';
import { ISong } from 'app/shared/model/song.model';

export interface IArtist {
  id?: number;
  artistName?: string;
  bio?: string | null;
  artistPicUrl?: string | null;
  albumIds?: IAlbum[] | null;
  songIds?: ISong[] | null;
}

export const defaultValue: Readonly<IArtist> = {};
