import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';

export interface IArticle {
  id?: number;
  text?: string;
  dateCreated?: string;
  articleAuthor?: IUser;
}

export const defaultValue: Readonly<IArticle> = {};
