import { IChordPic } from 'app/shared/model/chord-pic.model';
import { ISong } from 'app/shared/model/song.model';

export interface IInstrument {
  id?: number;
  name?: string;
  chordPics?: IChordPic[] | null;
  songIds?: ISong[] | null;
}

export const defaultValue: Readonly<IInstrument> = {};
