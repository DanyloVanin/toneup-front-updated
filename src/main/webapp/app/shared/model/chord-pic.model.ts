import { IChord } from 'app/shared/model/chord.model';
import { IInstrument } from 'app/shared/model/instrument.model';

export interface IChordPic {
  id?: number;
  pictureUrl?: string;
  chordId?: IChord;
  instrument?: IInstrument;
}

export const defaultValue: Readonly<IChordPic> = {};
