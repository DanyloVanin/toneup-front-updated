import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';
import { ISongRecord } from 'app/shared/model/song-record.model';

export interface IComment {
  id?: number;
  rating?: number;
  comment?: string | null;
  dateCreated?: string;
  commentAuthor?: IUser;
  song?: ISongRecord;
}

export const defaultValue: Readonly<IComment> = {};
