import React from 'react';
import { Translate } from 'react-jhipster';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import appConfig from 'app/config/constants';

export const BrandIcon = props => (
  <div {...props} className="brand-icon">
    <img src="content/images/note.png" alt="Logo" />
  </div>
);

export const Brand = props => (
  <NavbarBrand tag={Link} to="/" className="brand-logo">
    <BrandIcon />
    <span className="brand-title">
      <Translate contentKey="global.title"> ToneUp</Translate>
    </span>
  </NavbarBrand>
);

export const Home = props => (
  <NavItem>
    <NavLink tag={Link} to="/" className="d-flex align-items-center">
      <FontAwesomeIcon icon="home" />
      <span>
        <Translate contentKey="global.menu.home">Home</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Library = props => (
  <NavItem>
    <NavLink tag={Link} to="/library" className="d-flex align-items-center">
      <FontAwesomeIcon icon="bookmark" />
      <span>
        <Translate contentKey="global.menu.library">Library</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Tabs = props => (
  <NavItem>
    <NavLink tag={Link} to="/tabs" className="d-flex align-items-center">
      <FontAwesomeIcon icon="file-audio" />
      <span>
        <Translate contentKey="global.menu.tabs">Tabs</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Articles = props => (
  <NavItem>
    <NavLink tag={Link} to="/articles" className="d-flex align-items-center">
      <FontAwesomeIcon icon="newspaper" />
      <span>
        <Translate contentKey="global.menu.articles">Articles</Translate>
      </span>
    </NavLink>
  </NavItem>
);