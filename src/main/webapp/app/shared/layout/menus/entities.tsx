import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    data-cy="entity"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="compact-disc" to="/album">
      <Translate contentKey="global.menu.entities.album" />
    </MenuItem>
    <MenuItem icon="podcast" to="/artist">
      <Translate contentKey="global.menu.entities.artist" />
    </MenuItem>
    <MenuItem icon="icons" to="/genre">
      <Translate contentKey="global.menu.entities.genre" />
    </MenuItem>
    <MenuItem icon="music" to="/song">
      <Translate contentKey="global.menu.entities.song" />
    </MenuItem>
    <MenuItem icon="file-audio" to="/song-record">
      <Translate contentKey="global.menu.entities.songRecord" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/article">
      <Translate contentKey="global.menu.entities.article" />
    </MenuItem>
  </NavDropdown>
);
