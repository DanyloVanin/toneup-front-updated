import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
import album, { AlbumState } from 'app/entities/album/album.reducer';
import artist, { ArtistState } from 'app/entities/artist/artist.reducer';
import genre, { GenreState } from 'app/entities/genre/genre.reducer';
import song, { SongState } from 'app/entities/song/song.reducer';
import instrument, { InstrumentState } from 'app/entities/instrument/instrument.reducer';
import songRecord, { SongRecordState } from 'app/entities/song-record/song-record.reducer';
import tabs, {TabsState} from 'app/modules/tabs/tabs.reducer';
import chord, { ChordState } from 'app/entities/chord/chord.reducer';
import chordPic, { ChordPicState } from 'app/entities/chord-pic/chord-pic.reducer';
import userPlaylist, { UserPlaylistState } from 'app/entities/user-playlist/user-playlist.reducer';
import songInPlaylist, { SongInPlaylistState } from 'app/entities/song-in-playlist/song-in-playlist.reducer';
import comment, { CommentState } from 'app/entities/comment/comment.reducer';
import article, { ArticleState } from 'app/entities/article/article.reducer';

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly tabs: TabsState;
  readonly locale: LocaleState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly album: AlbumState;
  readonly artist: ArtistState;
  readonly genre: GenreState;
  readonly song: SongState;
  readonly instrument: InstrumentState;
  readonly songRecord: SongRecordState;
  readonly chord: ChordState;
  readonly chordPic: ChordPicState;
  readonly userPlaylist: UserPlaylistState;
  readonly songInPlaylist: SongInPlaylistState;
  readonly comment: CommentState;
  readonly article: ArticleState;
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  tabs,
  authentication,
  locale,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  album,
  artist,
  genre,
  song,
  instrument,
  songRecord,
  chord,
  chordPic,
  userPlaylist,
  songInPlaylist,
  comment,
  article,
  loadingBar,
});

export default rootReducer;
