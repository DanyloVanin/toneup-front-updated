# Pull official base image
FROM node:13.12.0-alpine AS builder
# Set working directory
WORKDIR /app
# Add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
# Install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install
# Copy app assets
COPY . ./
RUN npm run webapp:prod
# Define environment variables for Cloud Run
ENV PORT 8080
ENV HOST 0.0.0.0
EXPOSE 8080
# Start app
CMD ["node", "./server.js"]


